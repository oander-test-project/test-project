<?php

namespace Src\Exception;

use Exception;
use Src\Base\Handler;

class NotFoundException extends Exception
{
    public function __construct($message = null, $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, null);
    }
}