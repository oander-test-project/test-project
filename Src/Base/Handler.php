<?php

namespace Src\Base;

use Src\Exception\NotFoundException;
use Exception;
use Src\Traits\Resources;

class Handler
{

    use Resources;

    /**
     * @param $view
     * @param array $params
     * @param null $request
     * @param Exception|null $exception
     */
    public function render($view, array $params = [], $request = null, Exception $exception = null)
    {
        $view = $this->getViewPath(get_called_class(), $view);

        include_once $this->getHeaderPath() . '.php';

        if(file_exists("{$view}.php")) :
            include_once "{$view}.php";
        else :
            include_once $this->getHTTPNotFoundViewPath() . '.php';
        endif;

        include_once $this->getFooterPath() . '.php';
    }
}