<?php

namespace Src\Base;

interface MigrationInterface
{
    public function up();
    public function down();
}