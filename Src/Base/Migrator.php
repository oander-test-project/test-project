<?php

namespace Src\Base;

use Src\Database\DB;

abstract class Migrator
{
    abstract public function up();
    abstract public function down();

    protected function save() : object
    {
        $DB = new DB();
        $DB->query(($this->up()));

        return $DB;
    }
}