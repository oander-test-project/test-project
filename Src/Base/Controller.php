<?php

namespace Src\Base;

use Src\Traits\Routes;
use Src\Exception\NotFoundException;
use Src\Base\Handler;

class Controller extends Handler
{
    use Routes;

    /**
     * Controller constructor.
     * @throws NotFoundException
     * return action name when method is exits
     */
    public function __construct()
    {
        $action = "action{$this->action()}";
        if(method_exists(get_called_class(), $action))
            return $action;

        throw new NotFoundException('Controller method not found.');
    }

}