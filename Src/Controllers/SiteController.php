<?php

namespace Src\Controllers;

use Src\Base\Controller;
use Src\Database\DB;

Class SiteController extends Controller
{

    public function __construct()
    {
        // call valid action
        $this->{parent::__construct()}(parent::request());
    }

    public function actionIndex(array $request = [])
    {
        $DB = new DB();
        $limit = 10;

        $header = $DB
            ->query("SELECT * FROM object_model")
            ->fetchAll($DB::FETCH_ASSOC);

        $headerCols = count($header);
        $eavLimit = 0;
        $eavOffset = $limit * $headerCols;

        if(!empty($request) && is_numeric($request[0]) && $request[0] <= $headerCols && $request[0] > 0) {
            $eavLimit = $limit * $headerCols * $request[0];
        } else if (strstr($request[0], 'search')) {
            $searchElement = explode('&', ltrim($request[0], 'search?'));
        }

        $selectMonitors = "SELECT sm.sold_at, om.name, om.slug, me.* FROM sold_monitors as sm LEFT JOIN monitor_eav as me on me.monitor_id = sm.id JOIN object_model as om on om.id = me.object_model_id";
        $whereCondition = '';
        $isWhere = false;
//        $priceOrder = 'asc';
        if(isset($searchElement)) {
            $whereCondition .= " WHERE me.monitor_id IN (SELECT monitor_id FROM monitor_eav JOIN object_model on object_model.id = monitor_eav.object_model_id WHERE ";
            foreach ($searchElement as $searchItem) {
                $item = explode('=', $searchItem);
                $searchValues[$item[0]] = $item[1];
                if($item[1]) {
                    $isWhere = true;
                    $whereCondition .= " object_model.slug = '{$item[0]}' AND monitor_eav.value like '%{$item[1]}%'";
                    $whereCondition .= " OR";
                }
            }
            $whereCondition = rtrim($whereCondition, ' OR');
            $whereCondition .= ') ';
        }
        if ($isWhere) {
            $selectMonitors .= $whereCondition;
        }

        $selectMonitors .= " LIMIT {$eavLimit}, {$eavOffset}";

        $monitors = $DB
            ->query($selectMonitors)
            ->fetchAll($DB::FETCH_ASSOC);
        $countMonitors = $DB
            ->query("SELECT monitor_id FROM monitor_eav GROUP BY monitor_id")
            ->fetchAll($DB::FETCH_ASSOC);

        parent::render('index', [
            'monitors' => $monitors,
            'header' => $header,
            'countItem' => count($countMonitors),
            'headerCols' => $headerCols,
            'limit' => $limit,
            'page' => $request[0] ?? 1,
            'searchValues' => $searchValues ?: null,
        ], $request);
    }

}