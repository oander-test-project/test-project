<?php

namespace Src\Traits;

trait Resources
{

    /**
     * @return string
     */
    private function getPathPrefix() : string
    {
        return $_ENV['DOC_ROOT'] . '/Src/resources/views';
    }

    /**
     * @return string
     * return 404 page view file path
     */
    public function getHTTPNotFoundViewPath() : string
    {
        return $this->getPathPrefix() . DIRECTORY_SEPARATOR . 'site/404';
    }

    public function getViewPath(string $calledClass, string $action) : string
    {
        $class = explode('\\', $calledClass);
        $viewCategory = strtolower(str_replace('Controller', '', end($class)));
        $action = $this->getPathPrefix() . DIRECTORY_SEPARATOR . $viewCategory . DIRECTORY_SEPARATOR . $action;
        return $action;
    }

    public function getHeaderPath() : string
    {
        return $this->getPathPrefix() . DIRECTORY_SEPARATOR . '_partial/header';
    }

    public function getFooterPath() : string
    {
        return $this->getPathPrefix() . DIRECTORY_SEPARATOR . '_partial/footer';
    }
}