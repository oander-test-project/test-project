<?php

namespace Src\Traits;

trait Routes
{
    public function action() : string
    {
        $request = $this->uri();
        if(empty($request))
            return 'Index';

        return ucfirst(explode('/', $request)[0]);
    }

    public function request() : array
    {
        $request = explode('/', $this->uri());
        unset($request[0]);
        $request = array_values($request);
        return $request;
    }

    private function uri() : string
    {
        return ltrim($_SERVER['REQUEST_URI'], '/');
    }
}