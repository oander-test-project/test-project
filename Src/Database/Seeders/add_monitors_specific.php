<?php

namespace Src\Database\Seeders;

use Src\Base\MigrationInterface;
use Src\Base\Migrator;

class add_monitors_specific extends Migrator implements MigrationInterface
{
    public function __construct()
    {
        $this->save();
    }

    public function up() : string
    {
        return '
            INSERT INTO object_model (`name`, `slug`)
            VALUES
                ("Képátló", "diagonal"),
                ("Felbontás", "resolution"),
                ("Márka", "brand"),
                ("Ár", "price"),
                ("Akciós ár", "discount_price"),
                ("Név", "name"),
                ("Leírás", "description");
        ';
    }

    public function down() : string
    {
        return 'TRUNCATE object_model;';
    }

}