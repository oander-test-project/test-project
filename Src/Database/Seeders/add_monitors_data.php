<?php

namespace Src\Database\Seeders;

use Src\Base\MigrationInterface;
use Src\Base\Migrator;
use Src\Database\DB;

class add_monitors_data extends Migrator implements MigrationInterface
{

    public $brands = ['LG', 'Lenovo', 'Samsung', 'Sony', 'ASUS', 'AOC', 'Acer', 'HP', 'MSI'];
    public $resolutions = [240, 360, 480, 720, 1080, 1440, 2160];

    public function __construct()
    {
        $this->save();
    }

    public function up() : string
    {
        $values = $this->generateRow();
        $sql = 'INSERT INTO monitor_eav (`object_model_id`, `monitor_id`, `value`) VALUES ' . $values;

        return rtrim($sql, ',') . ';';
    }

    public function down() : string
    {
        return 'TRUNCATE monitor_eav;';
    }

    private function generateRow() : string
    {
        $DB = new DB();

        $values = '';

        $monitors = $DB->query("SELECT * FROM sold_monitors")->fetchAll($DB::FETCH_ASSOC);
        foreach ($monitors as $monitor) {
            $data = $this->generateMonitorData();
            foreach ($data as $specificKey => $specificValue) {
                $objectModels = $DB->query("SELECT id FROM object_model where slug = '{$specificKey}'")->fetch($DB::FETCH_ASSOC);
                $values .= '(' . $objectModels['id'] . ', ' . $monitor['id'] . ', "' . $specificValue . '"),';
            }
        }

        return $values;
    }

    private function generateMonitorData() : array
    {
        $resolution = $this->resolutions[array_rand($this->resolutions)];
        $brand = $this->brands[array_rand($this->brands)];
        $diagonal = rand(5, 75);

        $price = rand(75000, 250000);
        $discountPercent = rand(5, 50);
        return [
            'diagonal' => $diagonal,
            'resolution' => $resolution,
            'brand' => $brand,
            'price' => $price,
            'discount_price' => ($discountPercent / 100) * $price,
            'name' => "{$brand} {$diagonal}-{$resolution}p test monitor",
            'description' => $this->generateRandomString()
        ];
    }

    private function generateRandomString(int $length = 25) : string {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}