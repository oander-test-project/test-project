<?php

namespace Src\Database\Seeders;

use Src\Base\MigrationInterface;
use Src\Base\Migrator;

class add_monitors extends Migrator implements MigrationInterface
{

    public $brands = ['LG', 'Lenovo', 'Samsung', 'Sony', 'ASUS', 'AOC', 'Acer', 'HP', 'MSI'];
    public $resolutions = [240, 360, 480, 720, 1080, 1440, 2160];

    public function __construct()
    {
        $this->save();
    }

    public function up() : string
    {
        $sql = '
            INSERT INTO sold_monitors (`sold_at`) VALUES
        ';

        for ($productCount = 0; $productCount <= 75; $productCount++) {
            $int = mt_rand(1615818576, 1629037776);
            $soldAt = date("Y-m-d H:i:s", $int);
            $sql .= "('{$soldAt}'),";
        }
        return rtrim($sql, ',') . ';';
    }

    public function down() : string
    {
        return 'TRUNCATE sold_monitors;';
    }

}