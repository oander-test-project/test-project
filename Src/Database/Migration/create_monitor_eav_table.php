<?php

namespace Src\Database\Migration;

use Src\Base\MigrationInterface;
use Src\Base\Migrator;

class create_monitor_eav_table extends Migrator implements MigrationInterface
{
    public function __construct()
    {
        $this->save();
    }

    public function up()
    {
        return '
            CREATE TABLE IF NOT EXISTS monitor_eav
            (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                object_model_id INT(11) NOT NULL,
                monitor_id INT(11) NOT NULL,
                `value` TEXT DEFAULT NULL
            );
            
            CREATE INDEX idx_monitor_eav
            ON sold_monitors (id, `value`);
        ';
    }

    public function down()
    {
        return 'DROP TABLE IF EXISTS monitor_eav;';
    }
}