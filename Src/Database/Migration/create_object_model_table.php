<?php

namespace Src\Database\Migration;

use Src\Base\MigrationInterface;
use Src\Base\Migrator;

class create_object_model_table extends Migrator implements MigrationInterface
{
    public function __construct()
    {
        $this->save();
    }

    public function up()
    {
        return '
            CREATE TABLE IF NOT EXISTS object_model
            (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                name TEXT NOT NULL,
                slug VARCHAR(255),
                created_at datetime NOT NULL default CURRENT_TIMESTAMP
            );
            
            CREATE INDEX idx_object_model
            ON sold_monitors (id, name, slug);
        ';
    }

    public function down()
    {
        return 'DROP TABLE IF EXISTS object_model;';
    }
}