<?php

namespace Src\Database\Migration;

use Src\Base\MigrationInterface;
use Src\Base\Migrator;

class create_sold_monitors_table extends Migrator implements MigrationInterface
{
    public function __construct()
    {
        $this->save();
    }

    public function up()
    {
        return '
            CREATE TABLE IF NOT EXISTS sold_monitors
            (
                id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                created_at datetime NOT NULL default CURRENT_TIMESTAMP,
                updated_at datetime,
                sold_at datetime
            );
            
            CREATE INDEX idx_sold_monitors
            ON sold_monitors (id);
        ';
    }

    public function down()
    {
        return 'DROP TABLE IF EXISTS sold_monitors;';
    }
}