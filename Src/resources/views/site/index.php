<form method="get" action="/index/search">
    <table style="border: 1px solid;border-collapse: collapse;text-align: center;">
        <tr style="border-collapse: collapse;">
            <?php foreach ($params['header'] as $header) : ?>
                <th style="border: 1px solid; border-collapse: collapse; padding: 2px;" data-slug="<?= $header['slug'] ?>">
                    <?php if($header['slug'] == 'price') : ?>
                        <label style="font-size: 12px;"><input type="radio" name="<?= $header['slug'] ?>" value="0"> cheap</label><hr />
                        <label style="font-size: 12px;"><input type="radio" name="<?= $header['slug'] ?>" value="1"> expensive</label>
                    <?php else : ?>
                        <input type="text" value="<?= $params['searchValues'][$header['slug']] ?>" name="<?= $header['slug'] ?>">
                    <?php endif; ?>
                </th>
            <?php endforeach; ?>
        </tr>
        <tr style="border-collapse: collapse;">
    <!--        <th style="border: 1px solid; border-collapse: collapse; padding: 4px 10px;">#</th>-->
        <?php foreach ($params['header'] as $header) : ?>
            <th style="border: 1px solid; border-collapse: collapse; padding: 4px 10px;" data-slug="<?= $header['slug'] ?>"><?= $header['name'] ?></th>
        <?php endforeach; ?>
        </tr>
        <tr style="border-collapse: collapse;">
        <?php
            $monitorID = 0;
            foreach ($params['monitors'] as $monitor) :
                if($monitorID != $monitor['monitor_id'] && $monitorID != 0) :
        ?>
        </tr><tr>
                <?php endif; ?>
            <td style="border: 1px solid; border-collapse: collapse; padding: 4px 10px;"><?= $monitor['value'] ?></td>
        <?php
            $monitorID = $monitor['monitor_id'];
            endforeach;
        ?>
        </tr>
    </table>
    <input style="margin: 10px 0;" type="submit" value="Filter" />
</form>

<div style="float: left; border: 1px solid">
    <?php for ($p = 1; $p <= floor($params['countItem'] / $params['limit']); $p++) { ?>
        <?php if($params['page'] == $p) : ?>
        <span style="border-left: 1px solid;background: #ccc;padding: 7px;float: left"><?= $p ?></span>
        <?php else : ?>
        <span style="border-left: 1px solid;float: left"><a style="padding: 7px;float: left" href="/index/<?= $p ?>"><?= $p ?></a></span>
        <?php endif; ?>
    <?php } ?>
</div>