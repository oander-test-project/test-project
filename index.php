<?php

include_once 'Common/Config/app.php';

use Src\Base\Handler;
use \Src\Controllers\SiteController;
use Src\Exception\NotFoundException;

try {
    $site = new SiteController;
} catch (Exception $exception) {
    if ($exception instanceof NotFoundException) {
        $handler = new Handler();
        $handler->render(404, [],null, $exception);
    }
}