#Installation:
##Docker

##### Config container:

    - cp .env.example .env
    - Set project name in .env
    - cp access_info/.env.mysql.example access_info/.env.mysql

##### Config project [local]:

    - Set project `DOCROOT` in docker .env
    - Set mysql user:passwd in project config environment

##### Up project:

    - docker-compose up -d

## Project

##### Config project [local]:

    - nano Common/Config/environment.php

#### Run install (this step is created all table and uploads with data)
    - Open install.php ( {HOST_PORT_HTTP}/install.php )
 