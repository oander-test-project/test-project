<?php

    $_ENV = [
        'DB_HOST' => 'mysql',
        'DB_DATABASE' => 'test_db',
        'DB_USER' => 'root',
        'DB_PASSWORD' => 'topsecret',
        'DOC_ROOT' => $_SERVER["DOCUMENT_ROOT"]
    ];