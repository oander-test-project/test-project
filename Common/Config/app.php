<?php

// set basic error reporter
error_reporting(E_ALL);
ini_set("display_errors", 1);

// get test project environments
include_once __DIR__ . "/environment.php";
// use autoloader
include_once "{$_ENV['DOC_ROOT']}/Common/autoload.php";


