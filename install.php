<?php

include_once 'Common/Config/app.php';

header( 'Content-type: text/html; charset=utf-8' );
print 'Begin installation...<br />';

$migration = (string)'Src/Database/Migration';
$dataFixtures = (string)'Src/Database/Seeders';

print 'Migrate:<br />';
flush();
foreach (array_slice(scandir($migration), 2) as $key => $file) {
    $class = preg_replace('/\\.[^.\\s]{3,4}$/', '', $file);
    print $class . '<br />';
    flush();
    $migrationClass = new (str_replace('/', '\\', $migration) . '\\' .$class);
    try {
        $migrationClass->up();
        print 'Done<br /><br />';
        flush();
    } catch (Exception $e) {
        die($e->getMessage());
    }
}
flush();


print 'Data seed:<br />';
flush();

$seedMonitors = new Src\Database\Seeders\add_monitors;
$seedMonitors->up();

$seedMonitorsSpec = new Src\Database\Seeders\add_monitors_specific;
$seedMonitorsSpec->up();


$seedMonitorsData = new Src\Database\Seeders\add_monitors_data;
$seedMonitorsData->up();

print 'Done<br />';